import remove from 'unist-util-remove'
import visit from 'unist-util-visit'

function fixText (text) {
  return text
    .replace(/(?<=(^|[^\\])(\\{2})*)%.*?(\n|$)/g, '')
    .replace(/\\%/g, '%')
}

export default (opts) => {
  return (root) => {
    visit(root, 'text', (node, index, parent) => {
      node.value = fixText(node.value)
    })
    remove(root, { cascade: true }, (n) => n.type === 'text' && !n.value)
  }
}

export {
  // For testing
  fixText
}
