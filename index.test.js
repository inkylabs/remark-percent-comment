/* eslint-env jest */
import plugin, { fixText } from './index.js'

describe('fixText', () => {
  it('no percent', () => {
    expect(fixText('')).toEqual('')
    expect(fixText('This is a line')).toEqual('This is a line')
    expect(fixText('This is\ntwo lines')).toEqual('This is\ntwo lines')
  })

  it('escapes', () => {
    expect(fixText('\\%')).toEqual('%')
    expect(fixText('\\\\%')).toEqual('\\\\')
    expect(fixText('\\\\\\%')).toEqual('\\\\%')
    expect(fixText('A \\%')).toEqual('A %')
    expect(fixText('A \\\\%')).toEqual('A \\\\')
    expect(fixText('A \\\\\\%')).toEqual('A \\\\%')
    expect(fixText('\\% \\%')).toEqual('% %')
  })

  it('lines', () => {
    expect(fixText('%')).toEqual('')
    expect(fixText('% This is a test')).toEqual('')
    expect(fixText('This is %a test')).toEqual('This is ')
    expect(fixText('This %is a\ntest')).toEqual('This test')
    expect(fixText('This %is\na %test')).toEqual('This a ')
    expect(fixText('%This is\n%a test')).toEqual('')
  })
})

describe('plugin', () => {
  it('removal', () => {
    const p = plugin()
    let root = {
      children: [
        {},
        {
          type: 'plugin',
          children: [
            { type: 'text', value: 'blah' },
            { type: 'text', value: 'blah' }
          ]
        }
      ]
    }
    p(root)
    expect(root).toEqual({
      children: [
        {},
        {
          type: 'plugin',
          children: [
            { type: 'text', value: 'blah' },
            { type: 'text', value: 'blah' }
          ]
        }
      ]
    })
    root = {
      children: [
        {},
        {
          type: 'plugin',
          children: [
            { type: 'text', value: '%blah' },
            { type: 'text', value: 'blah' }
          ]
        }
      ]
    }
    p(root)
    expect(root).toEqual({
      children: [
        {},
        {
          type: 'plugin',
          children: [
            { type: 'text', value: 'blah' }
          ]
        }
      ]
    })
    root = {
      children: [
        {},
        {
          type: 'plugin',
          children: [
            { type: 'text', value: '%blah' },
            { type: 'text', value: '%blah' }
          ]
        }
      ]
    }
    p(root)
    expect(root).toEqual({
      children: [
        {}
      ]
    })
  })
})
