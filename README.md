# remark-percent-comment

TeX-style commenting in markdown.

## Syntax highlighting

### vim

Place this in `~/.vim/after/syntax/markdown.vim`:

```
syntax match htmlComment /%.*$/
syntax match htmlSpecialChar /\\[%\\]/
```
